package com.gl.vasgn.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.gl.vasgn.modelandview.BillerModal;
import com.gl.vasgn.modelandview.SubscribeModal;

@Component
public class BilerDAo {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public BillerModal bilerDetails(String msisdn)
	{
	
		String query="select biller_id,publisher,status from  com_subscribed_users_details where msisdn ='" + msisdn +"' AND status=1" ;
		System.out.println("query="+query);
		return jdbcTemplate.query(query, new  ResultSetExtractor<BillerModal>() {


			@Override
			public BillerModal extractData(ResultSet rs) throws SQLException, DataAccessException {
				//	landingPageModel list=new landingPageModel();
				//List<BillerModal> list=new ArrayList<BillerModal>();
				while(rs.next())
				{
					BillerModal billerModal=new BillerModal();
					//String id=String.valueOf(rs.getInt("pack_id"));
					billerModal.setBp(rs.getString("biller_id"));
					billerModal.setPublisher(rs.getString("publisher"));
					//billerModal.setStatus(1);
					billerModal.setStatus(rs.getInt("status"));	
					return 	billerModal;
					
				}
				  return null;
			}
			
		});
		
	}
	
	
	
	
	public List<SubscribeModal> subscribrdetails(String bp, String publisher)
	{
	
		String query="select name,price,duration from com_packs where biller_id='"+bp+"' AND publisher='"+publisher+"' AND status=1";
		System.out.println("query=="+query);
				return jdbcTemplate.query(query, new  ResultSetExtractor<List<SubscribeModal>>() {


			@Override
			public List<SubscribeModal> extractData(ResultSet rs) throws SQLException, DataAccessException {
				//	landingPageModel list=new landingPageModel();
				List<SubscribeModal> list=new ArrayList<SubscribeModal>();
				while(rs.next())
				{
					SubscribeModal billerModal=new SubscribeModal();
					//String id=String.valueOf(rs.getInt("pack_id"));
					billerModal.setPackname(rs.getString("name"));
					billerModal.setPrice(rs.getString("price"));
					billerModal.setDuration(rs.getInt("duration"));
					
					list.add(billerModal);
					
					
				}
				  return list;
			}
			
		});
		
	}
	
	public String fetchUrlToRedirect(String bp)
	{
	
		String query="select urlToRedirect from urlconfiguration where biller_id='"+bp+"'";
		System.out.println("query=="+query);
				return jdbcTemplate.query(query, new  ResultSetExtractor<String>() {


			@Override
			public String extractData(ResultSet rs) throws SQLException, DataAccessException {
				//	landingPageModel list=new landingPageModel();
				SubscribeModal billerModal=new SubscribeModal();
				while(rs.next())
				{
			
					billerModal.setUrlTrRedirect(rs.getString("urlToRedirect"));
					System.out.println("url="+billerModal.getUrlTrRedirect());
					  		
				}
				return billerModal.getUrlTrRedirect();
			
			}
			
		});
		
	}
}
