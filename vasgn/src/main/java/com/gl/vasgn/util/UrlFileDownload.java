package com.gl.vasgn.util;

import java.io.IOException;

public class UrlFileDownload {

	public static void main(String[] args) 
	{
		String fileURL = "https://s3.ap-south-1.amazonaws.com/way2ad/ContentImages/Redflower.jpg";
        String saveDir = "E:/";
        try {
        	//UtilityDownload.downloadFile(fileURL, saveDir);
        	
        	Download.saveImage(fileURL,saveDir);
        	
        } catch (IOException ex) {
            ex.printStackTrace();
        }
				
	}

}
