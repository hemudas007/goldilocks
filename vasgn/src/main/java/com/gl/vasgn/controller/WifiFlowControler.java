package com.gl.vasgn.controller;

import java.io.File;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.gl.vasgn.dao.BilerDAo;
import com.gl.vasgn.dao.Landingpage_Dao;
import com.gl.vasgn.dao.PanelDao;
import com.gl.vasgn.modelandview.BillerModal;
import com.gl.vasgn.modelandview.PanelContentModal;
import com.gl.vasgn.modelandview.SubscribeModal;
import com.gl.vasgn.modelandview.landingPageModel;
import com.gl.vasgn.util.Util;

@Controller
public class WifiFlowControler {

	@Autowired
	Landingpage_Dao landingpae_dao;

	@Autowired
	PanelDao panel;

	@Autowired
	BilerDAo bilerdao;

	/*
	 * @Value("${importer.imei.sample.filePath}") private String
	 * importerImeiSampleFilename;
	 * 
	 * @Value("${customs.imei.sample.filePath}") private String
	 * customsImeiSampleFilename;
	 */

	/*
	 * @Autowired Way2adDao way2ad;
	 */

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView view() throws Exception {
		System.out.println("reaching in biller or publisher details");
		return new ModelAndView("billerpage");
	}

	@RequestMapping(value = "/indexpage", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView indexpage(@RequestParam(required = false, name = "bp") String bp,
			@RequestParam(required = false, name = "publisher") String publisher,
			@RequestParam(required = false, name = "tokenid") String tokenid) throws Exception {
		List<PanelContentModal> list = panel.allPanelData();
//		System.out.println("complete list......................"+list);
		System.out.println("mobile number="+tokenid);
		Util util=new Util();
		//HashMap<String, List<PanelContentModal>> hashMap=util.getCategoryWiseContent(list);

		LinkedHashMap<String, List<PanelContentModal>> category = new LinkedHashMap<String, List<PanelContentModal>>();
		category=util.getCategoryWiseContent(list);
		//category.putAll(hashMap);

		System.out.println(".............................tree list............................................................");
		//System.out.println("category print"+category);


		ModelAndView mv = new ModelAndView("panel", "list",list );
		mv.addObject("category", category);

		if (publisher != null && tokenid.length()>=1  && bp != null) 
		{
			try
			{
			Base64.Decoder decoder = Base64.getDecoder();
			String dcodenumber = new String(decoder.decode(tokenid));
	
			System.out.println("decode mobilenumber="+dcodenumber);
			if(dcodenumber.length()==11 )
			{
				System.out.println("cecked sucessfully");
				BillerModal dbpublishers = bilerdao.bilerDetails(dcodenumber);
				if (dbpublishers.getStatus()==1) 
				{
					System.out.println("in true condition publisher=="+publisher+"  biller"+ bp);
					mv.addObject("publisher", publisher);
					mv.addObject("bp", bp);
					mv.addObject("status", "true");
				} else
				{

					System.out.println("in false condition");
					mv.addObject("status", "false");

				}
			}
			else if (dcodenumber.length()>=11 || dcodenumber.length()<=11)
			{
			System.out.println("nunber is less then or smaller then required formate");	
			}
			}
			catch (Exception ex)
			{
				System.out.println("exception "+ex);
			}
		
		}
		
		
		else if( bp != null && publisher != null && tokenid.length()<=0 )
				{
					System.out.println("else if condition=publisher ==="+publisher+"  biller=="+bp+"  token id="+tokenid.length());
					mv.addObject("publisher", publisher);
					mv.addObject("bp", bp);
					mv.addObject("status", "false");
				}

				else  if(bp == null && publisher == null )
				{
					String bpp="qa";
					String pbl="wwe";
					System.out.println("else condition");
					mv.addObject("od", bpp);
					mv.addObject("pbl", pbl);				
			}

		return mv;
	}

	@RequestMapping(value = "/Subscribe/bp={bp}/publisher={publisher}", method = RequestMethod.GET)
	public ModelAndView subcribe(@PathVariable String bp,@PathVariable String publisher) {


		System.out.println("in subscribe page.... ");
		System.out.println("in subscribe page....$$$$$ biller "+bp + "publisher" + publisher);

		List<SubscribeModal> subscribe=bilerdao.subscribrdetails(bp, publisher);
		String url=	bilerdao.fetchUrlToRedirect(bp);
		System.out.println("url****="+url);
		ModelAndView mv=	new ModelAndView();
		mv.setViewName("subscribe");
		mv.addObject("subscribe",subscribe);
		mv.addObject("url",url);
		
		return mv;

	}

	@RequestMapping(value = "/Subscriberdetail/bp={od}/publisher={pbl}", method = RequestMethod.GET)
	public ModelAndView subcribepage(@PathVariable String od,@PathVariable String pbl) {

		List<SubscribeModal> subscribe=bilerdao.subscribrdetails(od,pbl);
		System.out.println("in  subscribe page without tokenid.... ");
		System.out.println("in  subscribe details default  "+subscribe);
		return new ModelAndView("subscribe","subscribe",subscribe);

	}

	@RequestMapping(value = "/videopanelplayer/cnt_Id={id}", method = RequestMethod.GET)
	public ModelAndView player(@PathVariable int id) throws Exception {
		System.out.println("reaching in player page..");
		PanelContentModal paneel = panel.panelvideo(id);


		// System.out.println("video content id..."+id);

		// System.out.println("contents ......."+paneel);
		// String contentname=paneel.getContent_name();
		//String imagename = paneel.getImage_name();
		String baseformate = paneel.getBase_format();
		String videotype=paneel.getVideo_type();

		System.out.println("baseformate video ......." +baseformate );

		// String baseurl=baseformate.replaceFirst("176x144", "320x240");
		/* imagename.replaceAll("420x280", "320x240"); */

		//String imageurl = imagename.replaceFirst("420x280", "320x240");

		//System.out.println("replace character=== ......." + imageurl);
		ModelAndView mv = new ModelAndView("videopanelplayer", "imagename", baseformate);
		mv.addObject("videotype",videotype);
		return mv;
	}

	/*@RequestMapping(value = "/samples/{imagename:.+}", method = RequestMethod.GET)
	public ResponseEntity<?> imeiSampleFiles(@PathVariable String imagename, HttpServletRequest request,HttpServletResponse response) {

		String FILE_URL="http://s3.ap-south-1.amazonaws.com/way2ad/ContentImages/"+imagename;
		System.out.println("complete url"+FILE_URL);

		try (BufferedInputStream in = new BufferedInputStream(new URL(FILE_URL).openStream());
				  FileOutputStream fileOutputStream =new FileOutputStream(imagename)) {
			System.out.println("in try condition");

				    byte dataBuffer[] = new byte[1024];
				    int bytesRead;
				    while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
				        fileOutputStream.write(dataBuffer, 0, bytesRead);
				    }
				    System.out.println("after while condition");
			} 
		catch (IOException e) {
				    // handle exception
					System.out.println("exception: "+e);
				}

			return new ResponseEntity<>("Fail", HttpStatus.CONFLICT);

        }

	 */

	@RequestMapping(value = "/samples/{imagename}", method = RequestMethod.GET)
	public ResponseEntity<?> imeiSampleFiles(@PathVariable String imagename, HttpServletRequest request,
			HttpServletResponse response) {
		// requestRecievedMessage("imeiSampleFiles", fileType);
		System.out.println("this is  image name" + imagename);

		String csvFilePath = "/var/www/html/way2games/content/vi/"+ imagename+".apk";
	
		//String csvFilePath = "E:\\downloadvi" + imagename;
		System.out.println("complete path of video " + csvFilePath);

		try {

			File downloadFile = new File(csvFilePath);
			Util util = new Util();

			if (downloadFile.exists()) {
				util.downloadFileProperties(request, response, csvFilePath, downloadFile);
				System.out.println("this is  video download condition");
				return new ResponseEntity<>("Success", HttpStatus.OK);
			} else {
				/*
				 * if(logger.isDebugEnabled())
				 * logger.debug("Requested .CSV File Not Found At The Server ....!");
				 */
				return new ResponseEntity<>("Requested Video File Not Found At The Server ....!", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// logger.error(e);
			System.out.println(e);
			return new ResponseEntity<>("Fail", HttpStatus.CONFLICT);
		}
	}


	
	/*
@RequestMapping(value = "/samples/{imagename}", method = RequestMethod.GET)
public ResponseEntity<?> imeiSampleFiless(@PathVariable String imagename, HttpServletRequest request,
		HttpServletResponse response) {
	// requestRecievedMessage("imeiSampleFiles", fileType);
	System.out.println("this is  image name" + imagename);

	String csvFilePath = "/var/www/html/way2sport/content/vi/"+ imagename+".mp4";

	//String csvFilePath = "E:\\downloadvi" + imagename;
	System.out.println("complete path of video " + csvFilePath);

	try {

		File downloadFile = new File(csvFilePath);
		Util util = new Util();

		if (downloadFile.exists()) {
			util.downloadFileProperties(request, response, csvFilePath, downloadFile);
			System.out.println("this is  video download condition");
			return new ResponseEntity<>("Success", HttpStatus.OK);
		} else {

	 * if(logger.isDebugEnabled())
	 * logger.debug("Requested .CSV File Not Found At The Server ....!");

			return new ResponseEntity<>("Requested Video File Not Found At The Server ....!", HttpStatus.NOT_FOUND);
		}
	} catch (Exception e) {
		// logger.error(e);
		System.out.println(e);
		return new ResponseEntity<>("Fail", HttpStatus.CONFLICT);
	}
}

	 */






	@RequestMapping(value = "/landingpage", method = RequestMethod.GET)
	public ModelAndView vieww(@ModelAttribute landingPageModel landingpagemodal,
			@RequestParam("pack_id") String pack_id) throws Exception {

		landingPageModel lpm = landingpae_dao.landing_serviceId1(pack_id);
		ModelAndView mv = null;
		landingpagemodal.setPack_id(pack_id);
		String i = landingpagemodal.getPack_id();

		System.out.println("landing page dataa" + lpm);
		System.out.println("landing page id" + i);
		if (lpm == null) {
			mv = new ModelAndView("errorpage");
			System.out.println("thai");
			return mv;
		}

		String landingpagemsg = lpm.getLanding_page_message();

		System.out.println("+++**++-" + landingpagemsg);
		// String serviceid = lpm.getService_id();
		mv = new ModelAndView("game");
		mv.addObject("serviceid", i);
		mv.addObject("Aocpage", landingpagemsg);

		return mv;
	}



	@RequestMapping(value = "/category/{category}", method = RequestMethod.GET)
	public ModelAndView category(@PathVariable String category) 
	{
		System.out.println("in  scategory page "+category);

		List<PanelContentModal> morecategory=panel.categorydetail(category);
		System.out.println("single category details "+morecategory);
		return new ModelAndView("categorypage","list",morecategory);

	}




	@RequestMapping(value = "/aocpage", method = RequestMethod.GET)
	public ModelAndView view(@ModelAttribute landingPageModel landingpagemodal, @RequestParam("pack_id") String pack_id)
			throws Exception {
		landingpagemodal.setPack_id(pack_id);
		// String editorid = landingpagemodal.getService_id();

		landingPageModel lpm = landingpae_dao.landing_serviceId1(pack_id);
		landingPageModel compack = landingpae_dao.datafromCompack(landingpagemodal);

		ModelAndView mv = null;

		if (lpm == null) {
			mv = new ModelAndView("errorpage");
			System.out.println("thai");
			return mv;
		}

		String m = lpm.getAoc_page_message();
		System.out.println("+++++++++++++++" + m);
		String packdesc = lpm.getPack_description();
		String productid = lpm.getProduct_id();
		String packid = lpm.getPack_id();

		String serviceid = compack.getService_id();

		mv = new ModelAndView("gameaoc");
		System.out.println("in if condition");
		mv.addObject("Aocpage", m);
		mv.addObject("serviceid", packid);

		mv.addObject("packdesc", packdesc);
		mv.addObject("productid", productid);
		mv.addObject("service_id", serviceid);

		return mv;
	}
}

/*
 * @ResponseBody
 * 
 * @RequestMapping(value="/messageEditor",method=RequestMethod.GET) public
 * ModelAndView messageEditor(@ModelAttribute landingPageModel
 * landingPageModel,@RequestParam("service_id") String service_id) throws
 * Exception {
 * 
 * landingPageModel.setService_id(service_id);
 * 
 * String toid=landingPageModel.getService_id();
 * 
 * System.out.println(":::::::::::::::::::"+toid);
 * 
 * String bhel=req.getParameter("service_id");
 * 
 * 
 * landingpagemodal.setService_id(service_id); String
 * editorid=landingpagemodal.getService_id();
 * System.out.println("*+++++++++++++++++**"+editorid); ModelAndView mv = new
 * ModelAndView("messageEditor"); mv.addObject("toid",toid); return mv;
 * 
 * 
 * }
 */
