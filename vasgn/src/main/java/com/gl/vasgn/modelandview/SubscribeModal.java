package com.gl.vasgn.modelandview;

public class SubscribeModal {
	
	
	
	private String packname;
	private String price;
	private int duration;
	private String urlTrRedirect;
	public String getPackname() {
		return packname;
	}
	public void setPackname(String packname) {
		this.packname = packname;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
	public String getUrlTrRedirect() {
		return urlTrRedirect;
	}
	public void setUrlTrRedirect(String urlTrRedirect) {
		this.urlTrRedirect = urlTrRedirect;
	}
	
	
	@Override
	public String toString() {
		return "SubscribeModal [packname=" + packname + ", price=" + price + ", duration=" + duration + ",urlTrRedirect="+urlTrRedirect+"]";
	}
	
	
	
	
	
	

}
